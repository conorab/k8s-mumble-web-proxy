FROM library/debian:bullseye-slim
# libnice-dev, libssl-dev and clang are depenencies for mumble-web-proxy itself.
RUN apt -y update &&\
 apt -y upgrade &&\
 apt -y install libnice-dev libssl-dev clang git curl &&\
 git clone 'https://github.com/johni0702/mumble-web-proxy' &&\
 cd mumble-web-proxy &&\
 curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs --output /root/rustup-init.sh &&\
 chmod +x /root/rustup-init.sh &&\
 /root/rustup-init.sh -y --default-toolchain 1.45 &&\
 export PATH="/root/.cargo/bin:$PATH" &&\
 cargo build --release &&\
 chmod 755 target/release/mumble-web-proxy &&\
 chown root:root target/release/mumble-web-proxy &&\
 mv target/release/mumble-web-proxy /usr/local/bin &&\
 echo y | rustup self uninstall
ENV mumbleServerDomain=mumble.example.com
ENV mumbleServerPort=64738
ENV ICEPortMin=20000
ENV ICEPortMax=21000
ENV webSocketPort=64737
# Must be an IP; should be the IP which mumbleServerDomain points to.
ENV mumbleICEIP=192.168.0.2
CMD /usr/local/bin/mumble-web-proxy --listen-ws ${webSocketPort} --server ${mumbleServerDomain}:${mumbleServerPort} --ice-port-min $ICEPortMin --ice-port-max $ICEPortMax --ice-ipv4 $mumbleICEIP
EXPOSE ${webSocketPort}/tcp ${ICEPortMin}-${ICEPortMax}/tcp
